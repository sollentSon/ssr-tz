import type { AxiosRequestConfig, AxiosRequestHeaders } from 'axios'


export default interface IBaseHttpRepository {
    get(
        uri: string,
        queryParams?: string,
        headers?: AxiosRequestHeaders,
        config?: AxiosRequestConfig
    ): Promise<any>
}
