import BaseApiRepository from '@/repositories/BaseApiRepository'
import type BasicItemModel from '@/models/BasicItemModel'
import type AbstractResponse from '@/repositories/AbstractResponse'

class SearchApiRepository extends BaseApiRepository {

    public fetchByInput(searchString: string): Promise<BasicItemModel[]> {
        return this.get(`/search?${searchString}`,)
            .then((response: AbstractResponse<BasicItemModel[]>) => Promise.resolve(response.data))
            .catch((error: Error) => Promise.reject(error))
    }
}

export default new SearchApiRepository()
