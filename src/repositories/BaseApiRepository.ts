import axios from 'axios'
import type { AxiosInstance, AxiosRequestHeaders, AxiosRequestConfig } from 'axios'
import type IBaseHttpRepository from '@/repositories/IBaseHttpRepository'
import type AbstractResponse from '@/repositories/AbstractResponse'

export default abstract class BaseApiRepository implements IBaseHttpRepository {

    protected api: AxiosInstance
    protected baseUrl: string

    constructor() {
        this.baseUrl = import.meta.env.VITE_API_BASE_URL
        this.api = axios.create({baseURL: this.baseUrl})
    }


    private buildUrl(uri: string): string {
        return `${this.baseUrl}/${uri}`
    }

    public get(
        uri: string,
        queryParams?: string,
        headers?: AxiosRequestHeaders,
        config?: AxiosRequestConfig,
    ): Promise<AbstractResponse<any>> {
        return this.api.get(this.buildUrl(uri), config ?? {
            params: queryParams,
            headers: headers,
        });
    }
}
