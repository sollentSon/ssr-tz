export default interface AbstractResponse<M> {
    config: any
    data: M
    headers: any
    request: any
    status: number
    statusText?: string
}
