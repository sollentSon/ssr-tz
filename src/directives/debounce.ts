export function debounce<T extends Function>(cb: T, wait = 250): Function {
    let h = 0
    const callable = (...args: []) => {
        clearTimeout(h)
        h = setTimeout(() => cb(...args), wait)
    }
    return <T>(<Function>callable)
}
