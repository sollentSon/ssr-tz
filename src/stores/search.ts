import { defineStore } from 'pinia'
import { computed, ref } from 'vue'

export const useSearch = defineStore('useSearch', () => {
    const searchVar = ref('' as string)

    const search = computed({
        get() {
            return searchVar.value
        },
        set(v: string) {
            searchVar.value = v
        },
    })

    function change(v: string) {
        search.value = v
    }

    return {
        search,
        change,
    }
})
