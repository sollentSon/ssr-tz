import type Query from '@/models/Query'

export default new class QueryParamsGeneratorService {
    protected params: Query

    constructor() {
        this.params = {
            q: '',
            format: 'json',
            addressdetails: '1',
            polygon_geojson: '0'
        }
    }

    public generate = (searchString: string): string => {
        return new URLSearchParams(this.params = {...this.params, q: searchString,}).toString()
    }
}
