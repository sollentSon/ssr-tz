import { basename } from 'node:path'
import { createSSRApp } from 'vue'
import { renderToString } from 'vue/server-renderer'
import { createPinia } from 'pinia'
import App from './App.vue'


export async function render(url, manifest = null) {
    const app = createSSRApp(App)
    app.use(createPinia())

    const ctx = { modules: [] }
    const html = await renderToString(app)

    let preloadLinks = manifest ? renderPreloadLinks(ctx.modules, manifest) : ''

    return [html, preloadLinks]
}

function renderPreloadLinks(modules, manifest) {
    let links = ''
    const seen = new Set()
    modules.forEach((id) => {
        if (manifest[id]) {
            manifest[id].forEach((file) => {
                if (!seen.has(file)) {
                    seen.add(file)
                    if (manifest[basename(file)]) {
                        for (const depFile of manifest[basename(file)]) {
                            links += renderPreloadLink(depFile)
                            seen.add(depFile)
                        }
                    }
                    links += renderPreloadLink(file)
                }
            })
        }
    })
    return links
}

function renderPreloadLink(file) {
    if (file.endsWith('.js')) return `<link rel="modulepreload" crossorigin href="${file}">`
    else if (file.endsWith('.css')) return `<link rel="stylesheet" href="${file}">`
    else if (file.endsWith('.jpg') || file.endsWith('.jpeg')) return ` <link rel="preload" href="${file}" as="image" type="image/jpeg">`
    else if (file.endsWith('.png')) return ` <link rel="preload" href="${file}" as="image" type="image/png">`
    else return ''
}
