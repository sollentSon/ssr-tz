export default interface Query {
    q: string
    format: string
    addressdetails: string
    polygon_geojson: string
}
