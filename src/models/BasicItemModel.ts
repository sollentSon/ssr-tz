export default interface BasicItemModel {
    address: {
        "ISO3166-2-lvl4": string
        borough: string
        city: string
        country: string
        country_code: string
        historic: string
        house_number: number | string
        postcode: number | string
        road: string
        suburb: string
    }
    boundingbox: String[]
    class: string
    display_name: string
    importance: number | string
    lat: string | number
    licence: string
    lon: string | number
    osm_id: number| string
    osm_type: string
    place_id: number | string
    svg?: string
    type: string
}
