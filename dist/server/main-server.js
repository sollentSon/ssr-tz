var __defProp = Object.defineProperty;
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
var __publicField = (obj, key, value) => {
  __defNormalProp(obj, typeof key !== "symbol" ? key + "" : key, value);
  return value;
};
import { basename } from "node:path";
import { defineComponent, resolveComponent, mergeProps, withCtx, createTextVNode, useSSRContext, unref, ref, computed, watch, createVNode, resolveDynamicComponent, createSSRApp } from "vue";
import { ssrRenderAttrs, ssrRenderComponent, ssrInterpolate, ssrRenderAttr, ssrRenderVNode, ssrRenderList, renderToString } from "vue/server-renderer";
import { defineStore, createPinia } from "pinia";
import { useDark, useToggle } from "@vueuse/core";
import axios from "axios";
const _sfc_main$4 = /* @__PURE__ */ defineComponent({
  __name: "DayNightSwitcher",
  __ssrInlineRender: true,
  setup(__props) {
    const isDark = useDark({
      selector: "body",
      attribute: "class",
      valueDark: "dark",
      valueLight: ""
    });
    useToggle(isDark);
    return (_ctx, _push, _parent, _attrs) => {
      const _component_svg58style = resolveComponent("svg:style");
      _push(`<svg${ssrRenderAttrs(mergeProps({
        viewBox: "-10 -10 370 190",
        class: "day-night-switcher"
      }, _attrs))} data-v-9746961a>`);
      _push(ssrRenderComponent(_component_svg58style, {
        type: "text/css",
        "xmlns:svg": "http://www.w3.org/2000/svg"
      }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(` .sun, .moon { transition: opacity 0.3s linear; cursor: pointer; } .moon { opacity: 0; } .dark .moon { opacity: 1; } .dark .sun { opacity: 0; } `);
          } else {
            return [
              createTextVNode(" .sun, .moon { transition: opacity 0.3s linear; cursor: pointer; } .moon { opacity: 0; } .dark .moon { opacity: 1; } .dark .sun { opacity: 0; } ")
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(`<g class="moon" data-v-9746961a><rect x="-5" y="-5" width="360" height="180" rx="90" ry="90" stroke-width="10" fill="#2b2b2b" data-v-9746961a></rect><g fill="#FFF" data-v-9746961a><polygon points="180 33.25 181.58 39.42 187.75 41 181.58 42.58 180 48.75 178.42 42.58 172.25 41 178.42 39.42 180 33.25" data-v-9746961a></polygon><path d="M202.33,86.9a1.5,1.5,0,0,0,0-3,1.5,1.5,0,0,0,0,3Z" data-v-9746961a></path><path d="M177.59,152.21a1.5,1.5,0,0,0,0-3,1.5,1.5,0,0,0,0,3Z" data-v-9746961a></path><polygon points="222 110.5 225.17 122.83 237.5 126 225.17 129.17 222 141.5 218.83 129.17 206.5 126 218.83 122.83 222 110.5" data-v-9746961a></polygon><path d="M239,26.53a1.5,1.5,0,0,0,0-3,1.5,1.5,0,0,0,0,3Z" data-v-9746961a></path><polygon points="271 59.25 272.58 65.42 278.75 67 272.58 68.58 271 74.75 269.42 68.58 263.25 67 269.42 65.42 271 59.25" data-v-9746961a></polygon><path d="M278.53,136.38a1.5,1.5,0,0,0,0-3,1.5,1.5,0,0,0,0,3Z" data-v-9746961a></path><path d="M312.18,34.45a1.5,1.5,0,0,0,0-3,1.5,1.5,0,0,0,0,3Z" data-v-9746961a></path><path d="M328,107.68a1.5,1.5,0,0,0,0-3,1.5,1.5,0,0,0,0,3Z" data-v-9746961a></path></g><defs data-v-9746961a><mask id="moonMask" data-v-9746961a><rect x="0" y="0" width="350" height="170" rx="86" ry="86" fill="#FFF" stroke="#000" stroke-width="0" data-v-9746961a></rect><circle cx="122" cy="65" r="66" data-v-9746961a></circle></mask></defs><g mask="url(#moonMask)" data-v-9746961a><circle cx="88" cy="85" r="66" fill="#FFF" data-v-9746961a></circle></g></g><g class="sun" data-v-9746961a><rect x="-5" y="-5" width="360" height="180" rx="90" ry="90" stroke-width="10" fill="#85e8fe" data-v-9746961a></rect><circle cx="261" cy="85" r="66" fill="#FEC100" data-v-9746961a></circle><g fill="#FFF" data-v-9746961a><g data-v-9746961a><rect x="119.7" y="54.92" width="47.15" height="10.37" rx="5.19" ry="5.19" data-v-9746961a></rect><circle cx="125.4" cy="53.98" r="11.32" data-v-9746961a></circle><circle cx="144.8" cy="45.91" r="15.61" data-v-9746961a></circle><circle cx="160.8" cy="56.34" r="8.96" data-v-9746961a></circle></g><g data-v-9746961a><rect x="50" y="129" width="92.34" height="10.37" rx="5.19" ry="5.19" data-v-9746961a></rect><circle cx="134.53" cy="128.08" r="11.32" data-v-9746961a></circle><circle cx="82.65" cy="112.46" r="24.66" data-v-9746961a></circle><circle cx="114.35" cy="118.11" r="17.51" data-v-9746961a></circle><circle cx="54.21" cy="128.19" r="11.32" data-v-9746961a></circle></g></g></g></svg>`);
    };
  }
});
const DayNightSwitcher_vue_vue_type_style_index_0_scoped_9746961a_lang = "";
const _export_sfc = (sfc, props) => {
  const target = sfc.__vccOpts || sfc;
  for (const [key, val] of props) {
    target[key] = val;
  }
  return target;
};
const _sfc_setup$4 = _sfc_main$4.setup;
_sfc_main$4.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/components/DayNightSwitcher.vue");
  return _sfc_setup$4 ? _sfc_setup$4(props, ctx) : void 0;
};
const DayNightSwitcher = /* @__PURE__ */ _export_sfc(_sfc_main$4, [["__scopeId", "data-v-9746961a"]]);
const _sfc_main$3 = /* @__PURE__ */ defineComponent({
  __name: "SomePlace",
  __ssrInlineRender: true,
  props: {
    place: {
      type: Object,
      required: false,
      default: () => {
      }
    }
  },
  setup(__props) {
    const props = __props;
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "place bg-pan-left" }, _attrs))} data-v-4673532c><h3 class="place__title" data-v-4673532c>${ssrInterpolate(props.place.display_name)}</h3></div>`);
    };
  }
});
const SomePlace_vue_vue_type_style_index_0_scoped_4673532c_lang = "";
const _sfc_setup$3 = _sfc_main$3.setup;
_sfc_main$3.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/components/SomePlace.vue");
  return _sfc_setup$3 ? _sfc_setup$3(props, ctx) : void 0;
};
const SomePlace = /* @__PURE__ */ _export_sfc(_sfc_main$3, [["__scopeId", "data-v-4673532c"]]);
const _sfc_main$2 = /* @__PURE__ */ defineComponent({
  __name: "SearchPlaceholder",
  __ssrInlineRender: true,
  setup(__props) {
    const isDark = useDark();
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "placeholder" }, _attrs))} data-v-4bb08216><img${ssrRenderAttr("src", unref(isDark) ? "https://res.cloudinary.com/dq1c2xdfw/image/upload/v1674572600/search-5-dark_tvdv7r.svg" : "https://res.cloudinary.com/dq1c2xdfw/image/upload/v1675597049/search-5_awj0pe.svg")} alt="Placeholder" data-v-4bb08216></div>`);
    };
  }
});
const SearchPlaceholder_vue_vue_type_style_index_0_scoped_4bb08216_lang = "";
const _sfc_setup$2 = _sfc_main$2.setup;
_sfc_main$2.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/components/SearchPlaceholder.vue");
  return _sfc_setup$2 ? _sfc_setup$2(props, ctx) : void 0;
};
const SearchPlaceholder = /* @__PURE__ */ _export_sfc(_sfc_main$2, [["__scopeId", "data-v-4bb08216"]]);
const useSearch = defineStore("useSearch", () => {
  const searchVar = ref("");
  const search = computed({
    get() {
      return searchVar.value;
    },
    set(v) {
      searchVar.value = v;
    }
  });
  function change(v) {
    search.value = v;
  }
  return {
    search,
    change
  };
});
const _sfc_main$1 = /* @__PURE__ */ defineComponent({
  __name: "SearchBar",
  __ssrInlineRender: true,
  setup(__props) {
    useSearch();
    const modelValue = ref("");
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "search-container" }, _attrs))} data-v-7e4f4577><div class="search" data-v-7e4f4577><input type="text" class="search__input" id="input" placeholder="..."${ssrRenderAttr("value", modelValue.value)} autocomplete="off" data-v-7e4f4577><label for="input" class="search__label" data-v-7e4f4577><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" data-v-7e4f4577><path d="M12,2a8,8,0,0,0-8,8c0,5.4,7.05,11.5,7.35,11.76a1,1,0,0,0,1.3,0C13,21.5,20,15.4,20,10A8,8,0,0,0,12,2Zm0,17.65c-2.13-2-6-6.31-6-9.65a6,6,0,0,1,12,0C18,13.34,14.13,17.66,12,19.65ZM12,6a4,4,0,1,0,4,4A4,4,0,0,0,12,6Zm0,6a2,2,0,1,1,2-2A2,2,0,0,1,12,12Z" fill="#95959d" data-v-7e4f4577></path></svg></label></div></div>`);
    };
  }
});
const SearchBar_vue_vue_type_style_index_0_scoped_7e4f4577_lang = "";
const _sfc_setup$1 = _sfc_main$1.setup;
_sfc_main$1.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/components/SearchBar.vue");
  return _sfc_setup$1 ? _sfc_setup$1(props, ctx) : void 0;
};
const SearchBar = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["__scopeId", "data-v-7e4f4577"]]);
class BaseApiRepository {
  constructor() {
    __publicField(this, "api");
    __publicField(this, "baseUrl");
    this.baseUrl = "https://nominatim.openstreetmap.org";
    this.api = axios.create({ baseURL: this.baseUrl });
  }
  buildUrl(uri) {
    return `${this.baseUrl}/${uri}`;
  }
  get(uri, queryParams, headers, config) {
    return this.api.get(this.buildUrl(uri), config ?? {
      params: queryParams,
      headers
    });
  }
}
class SearchApiRepository extends BaseApiRepository {
  fetchByInput(searchString) {
    return this.get(`/search?${searchString}`).then((response) => Promise.resolve(response.data)).catch((error) => Promise.reject(error));
  }
}
const SearchApiRepository$1 = new SearchApiRepository();
const QueryParamsGenerateService = new class QueryParamsGeneratorService {
  constructor() {
    __publicField(this, "params");
    __publicField(this, "generate", (searchString) => {
      return new URLSearchParams(this.params = { ...this.params, q: searchString }).toString();
    });
    this.params = {
      q: "",
      format: "json",
      addressdetails: "1",
      polygon_geojson: "0"
    };
  }
}();
function debounce(cb, wait = 250) {
  let h = 0;
  const callable = (...args) => {
    clearTimeout(h);
    h = setTimeout(() => cb(...args), wait);
  };
  return callable;
}
const _sfc_main = /* @__PURE__ */ defineComponent({
  __name: "App",
  __ssrInlineRender: true,
  setup(__props) {
    const state = useSearch();
    const items = ref([]);
    let loadItems = debounce((search) => {
      SearchApiRepository$1.fetchByInput(QueryParamsGenerateService.generate(search)).then((res) => items.value = state.search ? res : []).catch((err) => console.warn(err));
    });
    watch(
      () => state.search,
      () => state.search.trim() === "" ? items.value = [] : loadItems(state.search)
    );
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<!--[--><header data-v-0964323f>`);
      ssrRenderVNode(_push, createVNode(resolveDynamicComponent(DayNightSwitcher), null, null), _parent);
      _push(ssrRenderComponent(SearchBar, null, null, _parent));
      _push(`</header><main data-v-0964323f>`);
      if (items.value.length) {
        _push(`<div class="places" data-v-0964323f><!--[-->`);
        ssrRenderList(items.value, (item) => {
          _push(ssrRenderComponent(SomePlace, {
            key: item.place_id,
            place: item
          }, null, _parent));
        });
        _push(`<!--]--></div>`);
      } else {
        _push(`<div data-v-0964323f>`);
        _push(ssrRenderComponent(SearchPlaceholder, null, null, _parent));
        _push(`</div>`);
      }
      _push(`</main><!--]-->`);
    };
  }
});
const App_vue_vue_type_style_index_0_scoped_0964323f_lang = "";
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/App.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const App = /* @__PURE__ */ _export_sfc(_sfc_main, [["__scopeId", "data-v-0964323f"]]);
async function render(url, manifest = null) {
  const app = createSSRApp(App);
  app.use(createPinia());
  const ctx = { modules: [] };
  const html = await renderToString(app);
  let preloadLinks = manifest ? renderPreloadLinks(ctx.modules, manifest) : "";
  return [html, preloadLinks];
}
function renderPreloadLinks(modules, manifest) {
  let links = "";
  const seen = /* @__PURE__ */ new Set();
  modules.forEach((id) => {
    if (manifest[id]) {
      manifest[id].forEach((file) => {
        if (!seen.has(file)) {
          seen.add(file);
          if (manifest[basename(file)]) {
            for (const depFile of manifest[basename(file)]) {
              links += renderPreloadLink(depFile);
              seen.add(depFile);
            }
          }
          links += renderPreloadLink(file);
        }
      });
    }
  });
  return links;
}
function renderPreloadLink(file) {
  if (file.endsWith(".js"))
    return `<link rel="modulepreload" crossorigin href="${file}">`;
  else if (file.endsWith(".css"))
    return `<link rel="stylesheet" href="${file}">`;
  else if (file.endsWith(".jpg") || file.endsWith(".jpeg"))
    return ` <link rel="preload" href="${file}" as="image" type="image/jpeg">`;
  else if (file.endsWith(".png"))
    return ` <link rel="preload" href="${file}" as="image" type="image/png">`;
  else
    return "";
}
export {
  render
};
